module eryajfgo

go 1.16

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-contrib/sessions v0.0.3
	github.com/gin-gonic/gin v1.6.3
	github.com/google/uuid v1.1.2
	github.com/mojocn/base64Captcha v1.3.1
	github.com/spf13/viper v1.9.0
	golang.org/x/crypto v0.0.0-20210817164053-32db794688a5
	gopkg.in/go-playground/validator.v8 v8.18.2
	gorm.io/driver/mysql v1.2.0
	gorm.io/gorm v1.22.3
)
